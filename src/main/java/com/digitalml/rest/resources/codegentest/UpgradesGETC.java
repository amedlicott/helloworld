package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for UpgradesGETC:
{
  "required": [
    "device"
  ],
  "type": "object",
  "properties": {
    "device": {
      "type": "object",
      "properties": {
        "deviceId": {
          "description": "Uniquely identifies a device",
          "type": "string"
        },
        "imei": {
          "description": "International Mobile Equipment Identifier.  Unique identifier for a mobile device on a network.",
          "type": "string"
        },
        "serialNumber": {
          "description": "Serial number of the equipment (device or accessory).",
          "type": "string"
        },
        "deviceStatus": {
          "description": "Status of the device.",
          "type": "string"
        },
        "blacklist": {
          "type": "object",
          "properties": {
            "statusCode": {
              "description": "Status code",
              "type": "string"
            },
            "subStatusCode": {
              "description": "Sub Status Code",
              "type": "string"
            },
            "reasonCode": {
              "description": "Status reason code",
              "type": "string"
            },
            "reasonDescription": {
              "description": "Status reason description",
              "type": "string"
            }
          }
        },
        "sku": {
          "description": "The sku of the device offer variant.  Only relevant if the customer purchased the device from T-Mobile.",
          "type": "string"
        },
        "offerId": {
          "description": "Uniquely identifies a product offering in the sales catalog.",
          "type": "string"
        },
        "offerType": {
          "description": "Primary categorization of Product Offering.",
          "type": "string"
        },
        "offerSubType": {
          "description": "Secondary categorization of Product Offering.",
          "type": "string"
        },
        "productType": {
          "description": "The category of product included in the offer: Plan, Service, Device, Accessory, or Financial.",
          "type": "string"
        },
        "productSubTypes": {
          "description": "Secondary categorizations of the type of product included in the offer.",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "offeringStatus": {
          "description": "Status of the product offering in the sales catalog.",
          "type": "string"
        },
        "offeringStatusPeriod": {
          "description": "The time period where the offeringStatus is valid.",
          "type": "object",
          "properties": {
            "startTime": {
              "type": "string",
              "format": "date-time"
            },
            "endTime": {
              "type": "string",
              "format": "date-time"
            }
          }
        },
        "businessUnit": {
          "type": "string"
        },
        "billingMethod": {
          "description": "The Billing Method specifies the billing experience that the customer has qualified for and requested. A change of the billing method implies the need to create a new billing arrangement.",
          "type": "string"
        },
        "brand": {
          "description": "The business name under which a TMUS product is sold.  A Customer can have accounts in any Brand, within the same Business Unit. For example, a T-Mobile customer may also have a branded account with a national retailer.",
          "type": "string"
        },
        "unitPrice": {
          "description": "Full list price per unit as set by T-Mobile",
          "type": "number",
          "format": "double"
        },
        "offerCategory": {
          "description": "Top-level display categorization for product offers.",
          "type": "string"
        },
        "offerSubCategory1": {
          "description": "Second-level display categorization for product offers.",
          "type": "string"
        },
        "offerSubCategory2": {
          "description": "Third-level display categorization for product offers.",
          "type": "string"
        },
        "offerName": {
          "description": "The commonly used name of the Offer, as defined in the Product Catalog.",
          "type": "string"
        },
        "offerDescription": {
          "description": "The description of the offer, as defined in the product catalog.",
          "type": "string"
        },
        "offerShortDescription": {
          "description": "The brief description of the offer, as defined in the product catalog.",
          "type": "string"
        },
        "offerLongDescription": {
          "description": "The verbose description of the offer, as defined in the product catalog.",
          "type": "string"
        },
        "displayOrder": {
          "description": "The sequence in which the offer is displayed.",
          "type": "integer",
          "format": "int32"
        },
        "billingTypes": {
          "description": "The type of billing account this offer is available to.  Could be prepaid and/or postpaid.",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "legalDisclosure": {
          "description": "Disclaimers or any other textual information that must be displayed to customers for legal reasons.",
          "type": "string"
        },
        "specifications": {
          "description": "Named value pairs",
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "name": {
                "type": "string"
              },
              "value": {
                "type": "string"
              }
            }
          }
        },
        "productId": {
          "description": "Uniquely identifies a known product.",
          "type": "string"
        },
        "productName": {
          "description": "The common name of the product, as defined in the product catalog.",
          "type": "string"
        },
        "productKeywords": {
          "description": "Words used in searches for the product.",
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "description": {
          "description": "Description of the product",
          "type": "string"
        },
        "shortDescription": {
          "description": "Short description of the product",
          "type": "string"
        },
        "longDescription": {
          "description": "Long description of the product",
          "type": "string"
        },
        "serviceProvider": {
          "description": "Service provider for the device",
          "type": "string"
        },
        "manufacturer": {
          "description": "Device manufacturer name",
          "type": "string"
        },
        "model": {
          "description": "Device model name",
          "type": "string"
        },
        "msrp": {
          "description": "The manufacturer's suggest retail price of the device.",
          "type": "number",
          "format": "double"
        },
        "purchasePrice": {
          "description": "The price the customer is actually charged for the equipement offer.",
          "type": "number",
          "format": "double"
        },
        "warrantyStatus": {
          "description": "Status of the warranty on the device.",
          "type": "string"
        },
        "estimatedPurchaseDate": {
          "description": "Estimated purchase date of the device.",
          "type": "string",
          "format": "date"
        },
        "registrationDate": {
          "description": "Registration date of the device.",
          "type": "string",
          "format": "date"
        },
        "warrantyExpirationDate": {
          "description": "Expiration date of the warranty on the device.",
          "type": "string",
          "format": "date"
        },
        "premiumCoverageVendor": {
          "description": "Vendor of the premium warranty coverage acquired by the customer, if any.",
          "type": "string"
        },
        "extendedCoveragePeriod": {
          "type": "object",
          "properties": {
            "startTime": {
              "type": "string",
              "format": "date-time"
            },
            "endTime": {
              "type": "string",
              "format": "date-time"
            }
          }
        },
        "manufacturerWarrantyDay": {
          "description": "OEM warranty days",
          "type": "integer",
          "format": "int32"
        },
        "manufacturerCoveragePeriod": {
          "description": "Coverage period by manufacturer",
          "type": "object",
          "properties": {
            "startTime": {
              "type": "string",
              "format": "date-time"
            },
            "endTime": {
              "type": "string",
              "format": "date-time"
            }
          }
        },
        "purchaseCountry": {
          "description": "Country where the device was purchased",
          "type": "string"
        },
        "daysRemaining": {
          "description": "Number of days the warranty is still in effect",
          "type": "integer",
          "format": "int32"
        },
        "maxTACReturnLimit": {
          "description": "Maximum number of time the TAC is allowed for return",
          "type": "integer",
          "format": "int32"
        },
        "numberOfTimesTACReturned": {
          "description": "Number of times the TAC has returned",
          "type": "integer",
          "format": "int32"
        },
        "accessoryWarrantyExpirationDate": {
          "description": "Warranty expiration date for adcessory associaed with the device ",
          "type": "string",
          "format": "date-time"
        },
        "recurringCharge": {
          "description": "The recurring charge amount the customer is charged for this device.",
          "type": "number",
          "format": "double"
        },
        "oneTimeCharge": {
          "description": "The one time charge the customer is charged for this device.",
          "type": "number",
          "format": "double"
        },
        "imsi": {
          "description": "An International Mobile Subscriber Identifier of IMSI is a unique number associated with all GSM and UMTS network mobile phone users.  It is stored in the SIM card.",
          "type": "string"
        },
        "simNumber": {
          "description": "Identifying number of the SIM on the network.",
          "type": "string"
        },
        "csn": {
          "description": "Customer Service Number for Apple SIM.",
          "type": "string"
        },
        "statusCode": {
          "description": "Status code",
          "type": "string"
        },
        "subStatusCode": {
          "description": "Sub Status Code",
          "type": "string"
        },
        "reasonCode": {
          "description": "Status reason code",
          "type": "string"
        },
        "reasonDescription": {
          "description": "Status reason description",
          "type": "string"
        },
        "unlockStatus": {
          "description": "Unlock status of the device",
          "type": "string"
        },
        "images": {
          "description": "Images associated with the product offering.",
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "imageId": {
                "description": "Unique identifier for the image PATH.  This name should be changed.",
                "type": "string"
              },
              "uri": {
                "description": "Universal resource identifier pointing to the image.",
                "type": "string"
              },
              "dimensions": {
                "description": "Dimensions of the image.",
                "type": "string"
              },
              "color": {
                "description": "Official name of the color of the equipment displayed in the image.",
                "type": "string"
              },
              "searchableColors": {
                "description": "Common color name for searching.",
                "type": "array",
                "items": {
                  "type": "string"
                }
              },
              "displayPurposes": {
                "description": "Purpose for which the image should be used in displays.",
                "type": "array",
                "items": {
                  "type": "string"
                }
              }
            }
          }
        },
        "deviceStartTime": {
          "description": "Date and time when the device was started for the customer.",
          "type": "string",
          "format": "date-time"
        },
        "numberOfDaysUsed": {
          "description": "Number of days the device is in use.",
          "type": "integer",
          "format": "int32"
        },
        "deviceEndTime": {
          "description": "Date and time when the device was ended for the customer.",
          "type": "string",
          "format": "date-time"
        },
        "msisdn": {
          "description": "Phone number ",
          "type": "string"
        },
        "networkUsePeriod": {
          "description": "The first and last time when the device used the network.",
          "type": "object",
          "properties": {
            "startTime": {
              "type": "string",
              "format": "date-time"
            },
            "endTime": {
              "type": "string",
              "format": "date-time"
            }
          }
        },
        "networkUseDays": {
          "description": "Number of days the device has used newwork.",
          "type": "integer",
          "format": "int32"
        },
        "tmoMerchandiseIndicator": {
          "description": "Indicates whether the customer originally purchased the device from T-Mobile.",
          "type": "boolean"
        },
        "originalPurchaseShipmentMethod": {
          "description": "Original purchase shipment method for the device.  in-store vs shipped",
          "type": "string"
        },
        "personalUnblockingKey": {
          "description": "Customer's personal unblocking key",
          "type": "string"
        },
        "acquisitionType": {
          "description": "To indicate how the device was acquired.",
          "type": "string"
        }
      }
    }
  }
}
*/

public class UpgradesGETC {

	@Size(max=1)
	@NotNull
	private com.digitalml.rest.resources.codegentest.Device device;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    device = new com.digitalml.rest.resources.codegentest.Device();
	}
	public com.digitalml.rest.resources.codegentest.Device getDevice() {
		return device;
	}
	
	public void setDevice(com.digitalml.rest.resources.codegentest.Device device) {
		this.device = device;
	}
}