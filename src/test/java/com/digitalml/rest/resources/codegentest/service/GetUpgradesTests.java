package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.HelloWorld.HelloWorldServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.HelloWorldService.GetUpgradesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.HelloWorldService.GetUpgradesReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetUpgradesTests {

	@Test
	public void testOperationGetUpgradesBasicMapping()  {
		HelloWorldServiceDefaultImpl serviceDefaultImpl = new HelloWorldServiceDefaultImpl();
		GetUpgradesInputParametersDTO inputs = new GetUpgradesInputParametersDTO();
		GetUpgradesReturnDTO returnValue = serviceDefaultImpl.getupgrades(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}