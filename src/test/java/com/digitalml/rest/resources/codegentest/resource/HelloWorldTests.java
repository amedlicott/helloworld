package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class HelloWorldTests {

	@Test
	public void testResourceInitialisation() {
		HelloWorldResource resource = new HelloWorldResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationGetUpgradesNoSecurity() {
		HelloWorldResource resource = new HelloWorldResource();
		resource.setSecurityContext(null);

		Response response = resource.getupgrades();
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetUpgradesNotAuthorised() {
		HelloWorldResource resource = new HelloWorldResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getupgrades();
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetUpgradesAuthorised() {
		HelloWorldResource resource = new HelloWorldResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getupgrades();
		Assert.assertEquals(404, response.getStatus());
	}

	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}